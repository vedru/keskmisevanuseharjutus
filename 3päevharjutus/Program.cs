﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3päevharjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> vanused = new List<int>();
            List<string> nimed = new List<string>();
            while (true)
            {
                Console.Write("Anna mulle õpilaste nimed: ");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                nimed.Add(nimi);
            }
            foreach (string nimi in nimed)
            {
                Console.Write($"Anna õpilase {nimi} vanus: ");
                vanused.Add(int.Parse(Console.ReadLine()));
            }
            int summa = 0;
            for (int i = 0; i < vanused.Count; i++) summa += vanused[i];
            Console.WriteLine($"Õpilaste keskmine vanus on {vanused.Average()}.");
            
        }
    }
}
